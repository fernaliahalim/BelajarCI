<?php

//disimpan di folder Controller dengan nama Home.php
//note yang harus diingat bahawa untuk penamaan kelas/definisi kelas huruf diawal harus kapital (CamelCase)
//penamaan file pada controller harus sama dengan nama kelas
class Home extends CI_Controller{
	public function index(){
		//ini untuk load view dengan nama file home.php
		$this->load->view('home.php');
	}
}