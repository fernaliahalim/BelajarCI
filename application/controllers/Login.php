<?php

class Login extends CI_Controller{
	public function __construct(){
		parent::__construct();

		//load model class
		//pemanggilan nama modelnya lowercase
		$this->load->model('login_model');
	}

	function index(){
		$data = array(
			"judul"	=> "CI Login Page"
		);
		$this->load->view('login', $data);
	}

	function proses_login(){
		//echo "ini adalah proses login";
		//menangkap post menggunakan PHP native
		//$user = $_POST['user'];
		
		//sekarang kita coba mengambil post menggunakan syntax CI
		//pertama-tama, kita definisikan dulu variabel yang akan
		//digunakan untuk menangkap post
		//misal kita gunakan variabel $params
		$params = $this->input->post(null, true);
		
		//sudah?
		//misal kita mau mengambil value dari name="user"
		$username	= $params['user']; //params harus sama dengan name yang di input
		//ambil value dari name="pass"
		$password	= $params['pass'];
		//oke silahkan diketikkan
		//sudah?
		//hanya bisa login jika user="admin" dan pass="admin"
		/*if($username == "admin" and $password == "admin"){
			echo "Berhasil Login";
		}else{
			echo "Gagal Login";
		}*/

		//cara memanggil fungsi login pada model Login_model.php
		$rs_login = $this->login_model->login($username, $password);

		//ini untuk cek berapa jumlah data yang dikembalikan oleh model
		//echo $rs_login->num_rows();

		//pengecekan username dan password sesuai dengan isi tabel atau bukan
		if($rs_login->num_rows()){
			//echo "Berhasil Login";

			//fetch data dari database
			$row_login = $rs_login->row(); //ini sama dengan mysql_fetch_array();

			//kalo berhasil login kita buat session untuk id_user, nama_user dan username
			$this->session->set_userdata('id_user', $row_login->id_user); //ini sama dengan $_SESSION['id_user'] = '';
			$this->session->set_userdata('nama_user', $row_login->nama_user);
			$this->session->set_userdata('username', $row_login->username);

			redirect('home');
		}else{
			//echo "Gagal Login";
			//kalo gaga login biarkan kembali ke halaman login 
			redirect('login');
		}
	}
}
?>