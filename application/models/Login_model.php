<?php

/*
	penamaan file diikuti dengan huruf kapital di awal
	sedangkan penamaan kelas mengikuti penamaan kelas di Controller (CamelCase)
*/
class Login_Model extends CI_Model{
	public function login($username, $password){
		/*
			di CI untuk melakukan komunikasi dengan tabel bisa dilakukan dengan banyak cara
			misalnya: 
			1. Statemant
			2. Prepared Statement
			3. Query Builder

			masing-masingnya punya keunggulan sendiri-sendiri. sebenernya sih dari tingkat securenya lebih tinggi
			yang Query Builder sama Prepared Statement dibanding Statement. Soalnya udah bisa handling SQL Injection
			
			jadi kalau cara komunikasi dengan tabelnya silahkan dipilih saja salah satu misalnya kita coba
			pakai Query Builder. Query Builder ini kelebihannya ga usah buat query kaya Statement dan Prepared Statement
			dan Query Builder bisa dibaca dokumentasinya di dokumentasi CI 
			di https://www.codeigniter.com/user_guide/database/query_builder.html?highlight=query%20builder
		*/

		//Statement
		//$sql = "SELECT * FROM user WHERE username = '{$username}' AND password = '{$password}'";
		//$query = $this->db->query($sql); //ini sama dengan mysql_query();

		//Prepared Statement
		//$sql = "SELECT * FROM user WHERE username = ? AND password = ?";
		//$query = $this->db->query($sql, array($username, $password));

		//Query Builder
		$this->db->where('username', $username); //ini sama dengan WHERE username = ?
		$this->db->where('password', $password); //ini sama dengan WHERE password = ?
		$query = $this->db->get('user'); //ini sama dengan SELECT * FROM user

		return $query;
	}
}